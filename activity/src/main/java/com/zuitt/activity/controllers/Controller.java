package com.zuitt.activity.controllers;

import com.zuitt.activity.models.User;
import com.zuitt.activity.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//Handles incoming HTTP requests and map them to the corresponding methods in the controller
@RestController

//CORS - Cross Origin Resource Sharing
//Allows request from a different domain to be made to the application
@CrossOrigin
public class Controller {
    @Autowired
    UserService userService;

    //Creating a user
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
    }

    //Getting all users
    @RequestMapping(value = "/users", method =  RequestMethod.GET)
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    //Deleting a user
    @RequestMapping(value = "/users/{userid}", method =  RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable Long userid) {
        return userService.deleteUser(userid);
    }

    //Updating a users
    @RequestMapping(value = "/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long userid, @RequestBody User user) {
        return userService.updateUser(userid, user);
    }
}
