package com.zuitt.activity.models;

import javax.persistence.*;

//Marks this Java object as a representation of an entity/record from the database table "posts"
@Entity
//Designates the table related to the model
@Table(name="posts")
public class Post {

    //Properties
    //Indicates that this property represents the primary key of the table
    @Id
    //Values for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    //Class properties that represent the table columns in a relational database are annotated as @Column
    @Column
    private String username;
    private String password;

    //Constructors
    public Post(){};

    public Post(String username, String password) {
        this.username = username;
        this.password = password;
    }

    //Getters and Setters
    public String getUsername(){
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String Password) {
        this.password = password;
    }

}
