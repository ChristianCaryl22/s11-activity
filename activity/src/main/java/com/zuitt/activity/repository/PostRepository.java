package com.zuitt.activity.repository;

import com.zuitt.activity.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//An interface marked as @Respository contains methods for database manipulation
@Repository

//By extending CrudRepository, Post Repository will inherit its pre-defined methods for creating, retrieving, updating, and deleting records
public interface PostRepository extends CrudRepository<Post, Object> {
}
