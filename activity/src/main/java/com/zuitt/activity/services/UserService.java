package com.zuitt.activity.services;

import com.zuitt.activity.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    void createUser(User user);

    Iterable<User> getUsers();

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long userid, User user);
}